package command

import (
	"fmt"
	"parking_lot-api/src/parking"
	"parking_lot-api/src/vehicle"
)

type (
	Command interface {
		Execute()
	}
	CreateParkingCommand struct {
		parking  *parking.Parking
		capacity int
	}
	ParkCommand struct {
		parking            *parking.Parking
		registrationNumber string
		color              string
	}
	UnParkCommand struct {
		parking   *parking.Parking
		slotIndex int
	}
	VehicleRegistrationByColorCommand struct {
		parking *parking.Parking
		color   string
	}
	VehicleSlotsByColorCommand struct {
		parking *parking.Parking
		color   string
	}

	VehicleSlotsByRegistrationNumberCommand struct {
		parking            *parking.Parking
		registrationNumber string
	}
)

func (cpc *CreateParkingCommand) Execute() {
	parking := cpc.parking
	parking.Create(cpc.capacity)
	fmt.Sprintf("Created a parking lot with %d slots\n", cpc.capacity)
}

func (pc *ParkCommand) Execute() {
	parking := pc.parking
	vehicle := vehicle.New(pc.registrationNumber, pc.color)
	slot, err := parking.Park(vehicle)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Sprintf("Allocated slot number: %d\n", slot.GetSlotIndex())
}

func (upc *UnParkCommand) Execute() {
	parking := upc.parking
	vehicle, err := parking.UnPark(upc.slotIndex)
	if err != nil {
		fmt.Sprintf("No vehicle found at %d\n", upc.slotIndex)
		return
	}
	if vehicle != nil {
		fmt.Sprintf("Slot number %d is free\n", upc.slotIndex)
	}
}

func (vc *VehicleRegistrationByColorCommand) Execute() {
	parking := vc.parking
	results := parking.GetVehicleRegistrationByColor(vc.color)
	var registrationNumbers []string
	for _, vehicle := range results {
		registrationNumbers = append(registrationNumbers, vehicle.GetRegistrationNumber())
	}
	fmt.Println(registrationNumbers)
}

func (vsc *VehicleSlotsByColorCommand) Execute() {
	parking := vsc.parking
	results := parking.GetVehicleParkedIndexSlotByColor(vsc.color)
	var slotIndex []int
	for _, slot := range results {
		slotIndex = append(slotIndex, slot.GetSlotIndex())
	}
	fmt.Println(slotIndex)
}

func (vsr *VehicleSlotsByRegistrationNumberCommand) Execute() {
	parking := vsr.parking
	results := parking.GetSlotNumberByVehicleRegistrationNumber(vsr.registrationNumber)
	var slotIndex []int
	for _, slot := range results {
		slotIndex = append(slotIndex, slot.GetSlotIndex())
	}
	fmt.Println(slotIndex)
}
