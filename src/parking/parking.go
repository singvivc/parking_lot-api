package parking

import (
	"errors"
	"parking_lot-api/src/vehicle"
)

type Parking struct {
	slot           map[int]Slot
	capacity       int
	availableSlots []int
}

func (p *Parking) Create(capacity int) {
	availableSlots := make([]int, capacity)
	maxLimit := capacity
	for idx, _ := range availableSlots {
		availableSlots[maxLimit-1] = idx + 1
		maxLimit--
	}
	p.slot = map[int]Slot{}
	p.capacity = capacity
	p.availableSlots = availableSlots
}

func (p *Parking) Park(vehicle vehicle.Vehicle) (Slot, error) {
	if len(p.availableSlots) == 0 {
		return nil, errors.New("parking not available")
	}
	occupiedSlotIndex := p.availableSlots[len(p.availableSlots)-1]
	p.availableSlots = p.availableSlots[0 : len(p.availableSlots)-1]
	slot := newSlot(0, occupiedSlotIndex, vehicle)
	p.slot[occupiedSlotIndex] = slot
	return slot, nil
}

func (p *Parking) UnPark(slotIndex int) (vehicle.Vehicle, error) {
	if occupiedSlot, ok := p.slot[slotIndex]; ok {
		delete(p.slot, slotIndex)
		p.availableSlots = append(p.availableSlots, slotIndex)
		return occupiedSlot.GetVehicle(), nil
	}
	return nil, errors.New("no vehicle is parked at the given slot index")
}

func (p *Parking) GetVehicleRegistrationByColor(color string) []vehicle.Vehicle {
	var vehicles []vehicle.Vehicle
	for _, occupiedSlot := range p.slot {
		if occupiedSlot.GetVehicle().GetColor() == color {
			vehicles = append(vehicles, occupiedSlot.GetVehicle())
		}
	}
	return vehicles
}

func (p *Parking) GetVehicleParkedIndexSlotByColor(color string) []Slot {
	var slots []Slot
	for _, occupiedSlot := range p.slot {
		if occupiedSlot.GetVehicle().GetColor() == color {
			slots = append(slots, occupiedSlot)
		}
	}
	return slots
}

func (p *Parking) GetSlotNumberByVehicleRegistrationNumber(registrationNumber string) []Slot {
	var slots []Slot
	for _, occupiedSlot := range p.slot {
		if occupiedSlot.GetVehicle().GetRegistrationNumber() == registrationNumber {
			slots = append(slots, occupiedSlot)
		}
	}
	return slots
}
