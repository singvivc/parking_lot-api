package parking

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

type stubVehicle struct {
	registrationNumber, color string
}

func (sb *stubVehicle) GetRegistrationNumber() string {
	return sb.registrationNumber
}

func (sb *stubVehicle) GetColor() string {
	return sb.color
}

func TestParking_Park(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")
	require.Equal(t, slot.slotIndex, 1)
	require.Equal(t, slot.level, 0)
	require.Equal(t, slot.vehicle.GetRegistrationNumber(), vehicle.registrationNumber)
	require.Equal(t, slot.vehicle.GetColor(), vehicle.color)
}

func TestParkingWithZeroCapacity_Park(t *testing.T) {
	parking := &Parking{}
	parking.Create(0)
	vehicle := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle)

	require.NotNil(t, err, "error object should not be nil")
	require.Nil(t, slot, "slot object is expected to be nil")
}

func TestParking_UnPark(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle1 := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle1)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	vehicle2 := stubVehicle{registrationNumber: "KA-01-HH-7777", color: "Red"}
	slot, err = parking.Park(&vehicle2)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	parkedVehicle, err := parking.UnPark(2)
	require.NotNil(t, parkedVehicle, "vehicle parked at slot 2 should be non nil")
	require.Equal(t, parkedVehicle.GetRegistrationNumber(), vehicle2.registrationNumber)
	require.Equal(t, parkedVehicle.GetColor(), vehicle2.color)
}

func TestUnParkingNonParkedVehicle(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle1 := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle1)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	parkedVehicle, err := parking.UnPark(2)
	require.NotNil(t, err, "error should be a non nil value")
	require.Equal(t, err.Error(), "no vehicle is parked at the given slot index")
	require.Nil(t, parkedVehicle, "parkedVehicle should be a nil value")
}

func TestParking_GetVehicleRegistrationByColor(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle1 := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle1)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	vehicle2 := stubVehicle{registrationNumber: "KA-01-HH-7777", color: "Red"}
	slot, err = parking.Park(&vehicle2)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	vehicles := parking.GetVehicleRegistrationByColor("White")
	assert.Equal(t, 1, len(vehicles))
}

func TestParking_GetVehicleParkedIndexSlotByColor(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle1 := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle1)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	vehicle2 := stubVehicle{registrationNumber: "KA-01-HH-7777", color: "Red"}
	slot, err = parking.Park(&vehicle2)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	slots := parking.GetVehicleParkedIndexSlotByColor("White")
	assert.Equal(t, 1, len(slots))
	assert.Equal(t, 1, slots[0].GetSlotIndex())
}

func TestParking_GetSlotNumberByVehicleRegistrationNumber(t *testing.T) {
	parking := &Parking{}
	parking.Create(2)
	vehicle1 := stubVehicle{registrationNumber: "KA-01-HH-1234", color: "White"}
	slot, err := parking.Park(&vehicle1)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	vehicle2 := stubVehicle{registrationNumber: "KA-01-HH-7777", color: "Red"}
	slot, err = parking.Park(&vehicle2)

	require.Nil(t, err, "error object should be null")
	require.NotNil(t, slot, "slot interface is expected to be non nil")

	slots := parking.GetSlotNumberByVehicleRegistrationNumber("KA-01-HH-7777")
	assert.Equal(t, 1, len(slots))
	assert.Equal(t, 2, slots[0].GetSlotIndex())
}
