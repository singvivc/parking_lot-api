package parking

import "parking_lot-api/src/vehicle"

type (
	slot struct {
		level     int
		slotIndex int
		vehicle   vehicle.Vehicle
	}

	Slot interface {
		GetLevel() int
		GetSlotIndex() int
		GetVehicle() vehicle.Vehicle
	}
)

func newSlot(level int, slotIndex int, vehicle vehicle.Vehicle) Slot {
	return &slot{level: 0, slotIndex: slotIndex, vehicle: vehicle}
}

func (s *slot) GetLevel() int {
	return s.level
}

func (s *slot) GetSlotIndex() int {
	return s.slotIndex
}

func (s *slot) GetVehicle() vehicle.Vehicle {
	return s.vehicle
}
