package vehicle

type (
	vehicle struct {
		registrationNumber string
		color              string
	}

	Vehicle interface {
		GetRegistrationNumber() string
		GetColor() string
	}
)

func New(registrationNumber, color string) Vehicle {
	return &vehicle{registrationNumber: registrationNumber, color: color}
}

func (v *vehicle) GetRegistrationNumber() string {
	return v.registrationNumber
}

func (v *vehicle) GetColor() string {
	return v.color
}
