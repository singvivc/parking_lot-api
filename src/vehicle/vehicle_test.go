package vehicle

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestVehicle_GetColor(t *testing.T) {
	vehicle := New("KA-01-HH-3141", "White")
	assert.Equal(t, "White", vehicle.GetColor())
}

func TestVehicle_GetRegistrationNumber(t *testing.T) {
	vehicle := New("KA-01-HH-3141", "White")
	assert.Equal(t, "KA-01-HH-3141", vehicle.GetRegistrationNumber())
}
